<?php

namespace App\Console\Commands;

use App\Jobs\ReportJob;
use App\Services\ReportService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class AverageDurationReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:average-duration
        {--queue : Whether the job should be queued}
        {--filepath= : which file to process (optional)}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gives Average duration across all bikes/stations in a given period';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        if (Storage::missing($this->getFilePath())) {
            $this->error('File is missing');
            return Command::FAILURE;
        }

        if ($this->option('queue')) {
            $this->queued();
        } else {
            $this->sync();
        }

        return Command::SUCCESS;
    }

    private function sync() : void
    {
        $service = (new ReportService());

        $format = $service->handle($this->getFilePath());

        $this->info('Average Time was: ' . $format);
    }

    private function queued() : void
    {
        ReportJob::dispatch($this->getFilePath());

        $this->info('You will get an email (check storage/logs/laravel.log)');
    }

    private function getFilePath() : string
    {
        // normally you would do this but hardcoding it otherwise will fail for you
        // return 'uploads/' . now()->subDay()->format('d-m-Y') . '.csv';

        return $this->option('filepath') ?? 'uploads/20-11-2021.csv';
    }
}
