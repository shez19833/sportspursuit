<?php

namespace App\Services;

use App\Libraries\Formats\FullTimeFormat;
use App\Libraries\Readers\CsvReader;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class ReportService
{
    public function handle($filepath) : string
    {
        [$count, $durationInseconds] = Cache::remember('report', now()->endOfDay(), function () use ($filepath) {
            return (new CsvReader())
                ->process($filepath);
        });

        return (new FullTimeFormat())
            ->handle($count, $durationInseconds);
    }
}
