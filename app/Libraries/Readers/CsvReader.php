<?php

namespace App\Libraries\Readers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\LazyCollection;

class CsvReader
{
    public function process($filepath) : array
    {
        $count = 0;
        $durationInseconds = 0;

        LazyCollection::make(function () use ($filepath) {
            $file = fopen(Storage::disk('local')->path($filepath), 'r');
            while ($data = fgetcsv($file)) {
                yield $data;
            }
        })->each(function ($data) use (&$count, &$durationInseconds) {
            $durationInseconds += Carbon::parse($data[3])
                ->diffInSeconds(Carbon::parse($data[2]));

            $count++;
        });

        return [$count, $durationInseconds];
    }
}
