<?php

namespace App\Libraries\Formats;

use Carbon\CarbonInterval;

class FullTimeFormat
{
    public function handle($count, $durationInseconds) : string
    {
        $result = $durationInseconds / $count;

        return CarbonInterval::seconds($result)
            ->cascade()
            ->format("%H:%I:%S");
    }
}
