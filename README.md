## Project

This is a test exercise to process a file & print average journey duration.

## Set up

You just need php installed on your local machine for mac its as easy as using brew. 

## Running

- install the repo on your local machine
- Go to your root directory of the project
- run composer install 
- copy .env.example to .env
- run `php artisan report:average-duration`
- optionally you can pass in a filepath 
    `php artisan report:average-duration --filepath=`
- optionally if you want it queued
    `php artisan report:average-duration --queue`
    Although for demo the queue is actually 'synced' anyway (in .env) otherwise you would need to run queue:work command etc
    Also for demo the result will be sent to storage/logs/laravel.log file and not email

## Testing

- in your root directory of the project
- run `php artisan test --filter=AverageDurationReportTest` 

## Assumptions

- it can be a large file so I am using caching/generators to keep the memory down
- Data is valid
- Going to assume its a report system where imports are uploaded every day (could be every week/monthly ie. not adhoc but for this test I will assume daily)
- Not sure about the ‘logic of working out average’ and whether the fact Arrival is empty means I should ‘use a default date of starting period’ and same with Departure (but as I think you are looking more at how I approach/structure the task I don’t want to get too bogged down on this) 
- Format will always be csv
- The test says ‘across all bikes and all stations’ which means I don’t have to group the times by bikes / stations e.g	
	station 1: 23:00:00	
    station 2: 11:00:00

Instead the answer should be just 34:00:00. 

## Rationale

So I know it seems a bit bizarre to use a fully fledged framework for this. But because it provides all the components that I wanted to work with out of the box, and I knew which features I needed, I just thought it would be easier to get going. You can of course create a standalone project and use composer to require only the packages that you need (Collections, Cache, etc etc)

I used Caching as the file is unlikely to change as its 'a given period' which I assume has already passed (e.g. last day)

I have also left some comments (more a todos)..
