<?php

namespace Tests\Feature\Commands;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

// possible tests for getFilePath
// make sure count / durations are correct
// tests for queue / sync options
class AverageDurationReportTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();

        Storage::disk('local')
            ->put('test.csv',
            "1,1,2021-11-01 05:00:00,2021-11-01 06:00:00
            2,2,2021-11-01 06:30:00,2021-11-01 07:00:00
            3,3,2021-11-01 08:15:10,2021-11-01 08:30:38");
    }

    public function test_average_time_is_correct()
    {
        $this->artisan('report:average-duration', ['--filepath' => 'test.csv'])
            ->expectsOutput('Average Time was: 00:35:09');
    }

    public function tearDown() : void
    {
        Storage::disk('local')
            ->delete('test.csv');

        parent::tearDown();
    }
}
